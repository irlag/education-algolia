import React, {Fragment} from 'react';
import { connectHierarchicalMenu } from 'react-instantsearch-dom';

const HierarchicalMenu = ({ items, refine, createURL }) => (
    <aside className="menu">
        <ul className="menu-list">
            {items.map(item => (
                <li key={item.label}>
                    <a
                        href={createURL(item.value)}
                        className={item.isRefined ? 'is-active' : '' }
                        onClick={event => {
                            event.preventDefault();
                            refine(item.value);
                        }}
                    >
                        {item.label} <span className="ais-RefinementList-count">{item.count}</span>
                    </a>
                    {item.items && (
                        <HierarchicalMenu
                            items={item.items}
                            refine={refine}
                            createURL={createURL}
                        />
                    )}
                </li>
            ))}
        </ul>
    </aside>
);

const CustomHierarchicalMenu = connectHierarchicalMenu(HierarchicalMenu);

export default CustomHierarchicalMenu;