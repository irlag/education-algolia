import React, {Fragment} from 'react';
import { connectPagination } from 'react-instantsearch-dom';

const Pagination = ({ currentRefinement, nbPages, refine, createURL }) => {
    return (
        nbPages > 1 &&
        <Fragment>
            <nav className="pagination is-rounded" role="navigation" aria-label="pagination">
                <ul className="pagination-list">
                    {
                        console.log(refine)
                    }
                    {new Array(nbPages).fill(null).map((_, index) => {
                        let linkClassNames = ['pagination-link'];
                        const page = index + 1;

                        if (currentRefinement === page) {
                            linkClassNames.push('is-current');
                        }

                        return (
                            <li key={index}>
                                <a
                                    href={createURL(page)}
                                    className={linkClassNames.join(' ')}
                                    onClick={event => {
                                        event.preventDefault();
                                        refine(page);
                                    }}
                                >
                                    {page}
                                </a>
                            </li>
                        );
                    })}
                </ul>
            </nav>
        </Fragment>
    )
};

const CustomPagination = connectPagination(Pagination);

export default CustomPagination;