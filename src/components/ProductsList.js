import React from 'react';

const ProductsList = ({ hit }) =>  {
    return (
        <div className="card">
            <div className="card-image">
                <figure className="image is-4by3">
                    <img src={hit.image} alt={hit.name} />
                </figure>
            </div>

            {/*<div className="card-content">
                <div className="media">
                    <div className="media-content">
                        <p className="title is-4">{hit.name}</p>
                        <p className="subtitle is-6">{hit.brand}</p>
                    </div>
                </div>

                <div className="content">
                    {hit.description}
                </div>
            </div>*/}
        </div>
    );
};

export default ProductsList;