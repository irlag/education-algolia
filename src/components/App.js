import React, {Fragment} from 'react';
import algoliasearch from 'algoliasearch/lite';
import {
    InstantSearch,
    SearchBox,
    Breadcrumb,
    Hits,
    HitsPerPage,
    RefinementList,
    HierarchicalMenu,
    Pagination,
    RangeInput,
    NumericMenu,
    ClearRefinements
} from 'react-instantsearch-dom';
import CustomSearchBox from "Components/CustomSearchBox";
import CustomBreadcrumb from "Components/CustomBreadcrumb";
import CustomHierarchicalMenu from "Components/CustomHierarchicalMenu";
import ProductsList from "Components/ProductsList";

const searchClient = algoliasearch(
    'latency',
    '6be0576ff61c053d5f9a3225e2a90f76'
);

const App = () => {
    const date = new Date();
    return (
        <Fragment>
            <section className="hero is-primary">
                <div className="hero-body">
                    <div className="container">
                        <h1 className="title">
                            Algolia Demo App
                        </h1>
                        <h2 className="subtitle">
                            Algolia is our way to convenient and powerful products.
                        </h2>
                    </div>
                </div>
            </section>

            <br />

            <InstantSearch searchClient={searchClient} indexName="instant_search">

                <div className="columns is-centered">
                    <div className="column is-four-fifths">
                        <CustomSearchBox />
                    </div>
                </div>

                <div className="columns is-centered">
                    <div className="column is-four-fifths">
                        <div className="columns">
                            <div className="column is-one-quarter">
                                <ClearRefinements />

                                <br />

                                <CustomHierarchicalMenu
                                    attributes={[
                                        'hierarchicalCategories.lvl0',
                                        'hierarchicalCategories.lvl1',
                                        'hierarchicalCategories.lvl2'
                                    ]}
                                />
                                {/*<RefinementList attribute="type" />*/}

                                <br />

                                <div className="tile notification is-warning">
                                    <RefinementList attribute="brand" limit={12} showMore showMoreLimit={25}/>
                                </div>

                                <div className="tile notification is-warning">
                                    <RefinementList attribute="price_range" />
                                </div>
                                {/*<RangeInput
                                    attribute="price"
                                    min={10}
                                    max={100}
                                />
                                <NumericMenu
                                    attribute="price"
                                    items={[
                                        { label: '<= $10', end: 10 },
                                        { label: '$10 - $100', start: 10, end: 100 },
                                        { label: '$100 - $500', start: 100, end: 500 },
                                        { label: '>= $500', start: 500 },
                                    ]}
                                />*/}

                            </div>
                            <div className="column">
                                <CustomBreadcrumb
                                    attributes={[
                                        'hierarchicalCategories.lvl0',
                                        'hierarchicalCategories.lvl1',
                                        'hierarchicalCategories.lvl2'
                                    ]}
                                />

                                <Hits hitComponent={ProductsList}/>

                                <br />

                                <div className="columns">
                                    <div className="column is-one-quarter">
                                        <HitsPerPage
                                            defaultRefinement={16}
                                            items={[
                                                { value: 8, label: 'Show 8 hits' },
                                                { value: 16, label: 'Show 16 hits' },
                                                { value: 24, label: 'Show 24 hits' },
                                            ]}
                                        />
                                    </div>
                                    <div className="column">
                                        <Pagination showLast />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </InstantSearch>

            <footer className="footer">
                <div className="content has-text-centered">
                    <p>
                        <strong>Algolia demo app</strong> by <a href="mailto:irlag@yandex.ru">Acamaz Naldikoev</a>. The source code is licensed <a href="http://opensource.org/licenses/mit-license.php">MIT</a>.
                        {/*The website content is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY NC SA 4.0</a>.*/}
                        <br />
                        &copy; {date.getFullYear()}
                    </p>
                </div>
            </footer>
        </Fragment>
    )
};

export default App;