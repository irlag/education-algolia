import React from 'react';
import { connectSearchBox } from 'react-instantsearch-dom';

const SearchBox = ({ currentRefinement, isSearchStalled, refine }) => (
    <form noValidate action="" role="search">
        <input
            type="search"
            placeholder="Start searching..."
            className="input"
            value={currentRefinement}
            onChange={event => refine(event.currentTarget.value)}
        />
        {isSearchStalled ? 'My search is stalled' : ''}
    </form>
);

const CustomSearchBox = connectSearchBox(SearchBox);

export default CustomSearchBox;