namespace :yarn do
    task :build do
        on roles fetch(:yarn_roles) do
            within fetch(:yarn_target_path, release_path) do
                with fetch(:yarn_env_variables, {}) do
                  execute fetch(:yarn_bin), 'build:prod'
                end
            end
        end
    end
end