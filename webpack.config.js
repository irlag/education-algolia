const webpack = require('webpack');
const path = require('path');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';

let config = {
    context: path.resolve(__dirname, './src'),
    entry: {
        app: './app'
    },
    output: {
        path: path.resolve(__dirname, './public/dist'),
        //filename: '[name].[chunkhash].js'
        filename: '[name].js'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules')
        ],
        alias: {
            Components: path.resolve(__dirname, './src/components'),
            Config: path.resolve(__dirname, './src/config'),
            Templates: path.resolve(__dirname, './src/templates')
        }
    },
    mode: NODE_ENV == 'development' ? 'development' : 'production',
    devtool: NODE_ENV == 'development' ? 'source-map' : false,
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env', '@babel/preset-react']
                        }
                    }
                ]
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.css$/,
                include: /(node_modules)/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            // you can specify a publicPath here
                            // by default it use publicPath in webpackOptions.output
                            //publicPath: '../'
                        }
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            import: false,
                            url: true
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'images/'
                            //publicPath: '/assets/'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
            //_: 'lodash'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css'
            //allChunks: true
            // chunkFilename: "[id].css"
        }),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        })
    ],
    node: {
        console: false,
        dgram: 'empty',
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        child_process: 'empty'
    }
};

if (NODE_ENV == 'production') {
    config.optimization = {
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    cache: true,
                    parallel: true,
                    sourceMap: true,
                    beautify: false,
                    ie8: false,
                    compress: {
                        warnings: false,
                        drop_console: false,
                        unsafe: true
                    },
                    comments: false
                }
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    }
} else {
    /*const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

    config.plugins.push(
        new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            openAnalyzer: false
        })
    );*/
}

module.exports = config;
